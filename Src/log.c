/*
 * @File: log.c
 * @Author: xusongbin@appotronics.cn
 * @Descripttion: Format the log print
 * @version: v1.0.0
 * @Date: 2021-06-19 11:32:34
 * @LastEditors: heqinglong@appotronics.cn
 * @LastEditTime: 2021-12-17 14:13:54
 * @Copyright: Copyright (c) 2020
 */

#include "log.h"

/*---------设置log等级:QUIET ERR WARN INFO DEBUG-------------*/
static ae_log_level log_level = INFO;

/**
 * @function: log_set_level
 * @description: Set log level
 * @param {ae_log_level} level
 * @return {*}
 * @record: 
 */
void log_set_level(ae_log_level level)
{
    log_level = level;
}

/**
 * @function: log_get_level
 * @description: Get log level
 * @param {*}
 * @return {*}
 * @record: 
 */
ae_log_level log_get_level(void)
{
    return log_level;
}

const char *log_level_str[] = {
	"QUIET",
    "ERROR",
    "WARN",
    "INFO",
    "DEBUG",
};

/**
 * @function: log_puts
 * @description: Parses and outputs the printed information
 * @param {ae_log_level} level
 * @param {const char} *module
 * @param {char} *fmt
 * @return {*}
 * @record: 
 */
int log_puts(ae_log_level level, const char *module,char *fmt, ...)
{
    int d, len, j;
    char c, *s;
    uint8_t *hbuf;
    int32_t *ibuf;
    uint32_t *uibuf;
    double lf;
    char strfmt[10];
    va_list ap;
    FILE *f = stdout;
/*
**1:只打印设置等级的log,
**2:log等级为QUIET则关闭所有log
**3:参数为空则不打印
*/
    if((level != log_level)||(log_level == QUIET)||(fmt == NULL)){return 0;}

    va_start(ap, fmt);

    fprintf(f, "[$%s $%s] ", log_level_str[level],module);

    if(fmt != NULL){
        va_start(ap, fmt);
        while(*fmt){
            if(*fmt == '%'){
                strfmt[0] = '%';
                j=1;
                while( ( fmt[j]>='0' && fmt[j]<='9' ) || \
                      ( fmt[j]== '-' ) || ( fmt[j]== '+' ) || ( fmt[j]== '.' ) ){
                          strfmt[j] = fmt[j];
                          j++;
                      }
                strfmt[j] = fmt[j];
                fmt += j;
                j++;
                strfmt[j] = '\0';

                switch(*fmt){
                case '%':
                    fprintf(f, strfmt);
                    break;
                case 'u':
                    d = va_arg(ap, int);
                    fprintf(f, strfmt, (uint32_t)d);
                    break;
                case 'd':
                case 'x':
                case 'X':
                    d = va_arg(ap, int);
                    fprintf(f, strfmt, d);
                    break;
                case 'i':
                    ibuf = va_arg(ap, int32_t *);
                    len = va_arg(ap, int);
                    for(d=0; d<len; d++){
                        fprintf(f, "%d ", ibuf[d]);
                    }
                    break;
                case 'I':
                    uibuf = va_arg(ap, uint32_t *);
                    len = va_arg(ap, int);
                    for(d=0; d<len; d++){
                        fprintf(f, "%10d ", uibuf[d]);
                    }
                    break;
                case 'h':
                case 'H':
                    hbuf = va_arg(ap, uint8_t *);
                    len = va_arg(ap, int);
                    for(d=0; d<len; d++){
                        if(*fmt == 'h'){
                            fprintf(f, "%02X", hbuf[d]);
                        }else{
                            fprintf(f, "%02X ", hbuf[d]);
                        }
                    }
                    break;
                case 'n':
                    hbuf = va_arg(ap, uint8_t *);
                    len = va_arg(ap, int);
                    for(d=0; d<len; d++){
                        fprintf(f, "%02X", hbuf[d]);
                        if(d != (len-1)){
                            fputc(':', f);
                        }
                    }
                    break;
                case 's':
                    s = va_arg(ap, char *);
                    fprintf(f, strfmt, s);
                    break;
                case 'c':
                    c = (char)va_arg(ap, int);
                    fprintf(f, strfmt, c);
                    break;
                case 'f':
                    lf = va_arg(ap, double);
                    fprintf(f, strfmt, lf);
                    break;
                }
                fmt++;
            }else{
                fputc(*fmt++, f);
            }
        }
        va_end(ap);
    }
    fprintf(f, "\r\n");

    return 0;
}


