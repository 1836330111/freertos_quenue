/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define NAME    "Freertos"

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */

/* USER CODE END Variables */
/* Definitions for defaultTask */
osThreadId_t defaultTaskHandle;
const osThreadAttr_t defaultTask_attributes = {
  .name = "defaultTask",
  .priority = (osPriority_t) osPriorityNormal,
  .stack_size = 128 * 4
};
/* Definitions for myTask02 */
osThreadId_t myTask02Handle;
const osThreadAttr_t myTask02_attributes = {
  .name = "myTask02",
  .priority = (osPriority_t) osPriorityLow,
  .stack_size = 128 * 4
};
/* Definitions for myTask03 */
osThreadId_t myTask03Handle;
const osThreadAttr_t myTask03_attributes = {
  .name = "myTask03",
  .priority = (osPriority_t) osPriorityLow,
  .stack_size = 128 * 4
};
/* Definitions for Task4 */
osThreadId_t Task4Handle;
const osThreadAttr_t Task4_attributes = {
  .name = "Task4",
  .priority = (osPriority_t) osPriorityLow,
  .stack_size = 128 * 4
};
/* Definitions for Usart1_Queue1 */
osMessageQueueId_t Usart1_Queue1Handle;
const osMessageQueueAttr_t Usart1_Queue1_attributes = {
  .name = "Usart1_Queue1"
};
/* Definitions for Usart2_Queue1 */
osMessageQueueId_t Usart2_Queue1Handle;
const osMessageQueueAttr_t Usart2_Queue1_attributes = {
  .name = "Usart2_Queue1"
};
/* Definitions for myMutex01 */
osMutexId_t myMutex01Handle;
const osMutexAttr_t myMutex01_attributes = {
  .name = "myMutex01"
};

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */

/* USER CODE END FunctionPrototypes */

void StartDefaultTask(void *argument);
void StartTask02(void *argument);
void Task3(void *argument);
void StartTask04(void *argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */

  /* USER CODE END Init */
  /* Create the mutex(es) */
  /* creation of myMutex01 */
  myMutex01Handle = osMutexNew(&myMutex01_attributes);

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* Create the queue(s) */
  /* creation of Usart1_Queue1 */
  Usart1_Queue1Handle = osMessageQueueNew (5, 50, &Usart1_Queue1_attributes);

  /* creation of Usart2_Queue1 */
  Usart2_Queue1Handle = osMessageQueueNew (5, 50, &Usart2_Queue1_attributes);

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* creation of defaultTask */
  defaultTaskHandle = osThreadNew(StartDefaultTask, NULL, &defaultTask_attributes);

  /* creation of myTask02 */
  myTask02Handle = osThreadNew(StartTask02, NULL, &myTask02_attributes);

  /* creation of myTask03 */
  myTask03Handle = osThreadNew(Task3, NULL, &myTask03_attributes);

  /* creation of Task4 */
  Task4Handle = osThreadNew(StartTask04, NULL, &Task4_attributes);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* USER CODE BEGIN RTOS_EVENTS */
  /* add events, ... */
  /* USER CODE END RTOS_EVENTS */

}

/* USER CODE BEGIN Header_StartDefaultTask */
/**
  * @brief  Function implementing the defaultTask thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void *argument)
{
  /* USER CODE BEGIN StartDefaultTask */
  /* Infinite loop */
  for(;;)
  {
//	printf("systerm running \r\n");  
    osDelay(10);
  }
  /* USER CODE END StartDefaultTask */
}

/* USER CODE BEGIN Header_StartTask02 */
/**
* @brief Function implementing the myTask02 thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartTask02 */
void StartTask02(void *argument)
{
  /* USER CODE BEGIN StartTask02 */
	uint8_t SPI_WBUFFER[]={0x01,0x02,0x03,0x04};
	uint8_t IIC_WBUFFER[]={0x04,0x03,0x02,0x01};
	uint8_t SPI_read[50];
	uint8_t IIC_read[50];
	uint8_t i;
  /* Infinite loop */
  for(;;)
  {
	  BSP_W25Qx_Write((uint8_t*)SPI_WBUFFER,0x00,4);
	  BSP_W25Qx_Read(SPI_read,0x00,4);
	  printf("[buffer]: %d %d %d %d \n",SPI_read[0],SPI_read[1],SPI_read[2],SPI_read[3]);	

	  
	  AT24CXX_Write(0,(uint8_t*)IIC_WBUFFER,4);
	  AT24CXX_Read(0,IIC_read,4);
	  printf("buffer %d %d %d %d \n",IIC_read[0],IIC_read[1],IIC_read[2],IIC_read[3]);

//	printf("task2:%d \r\n",strlen((const char*)buffer));	
//	for (i = 0;i<strlen((const char*)buffer);i++)
//	printf("%c",buffer[i]);
    osDelay(1000);
	vTaskDelay(20);
  }
  /* USER CODE END StartTask02 */
}

/* USER CODE BEGIN Header_Task3 */
/**
* @brief Function implementing the myTask03 thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_Task3 */
void Task3(void *argument)
{
  /* USER CODE BEGIN Task3 */
	uint8_t buffer[50];
	uint8_t i;
	BaseType_t xTaskWokenByReceive=pdFALSE;
  /* Infinite loop */
  for(;;)
  {

        if(Usart1_Queue1Handle!=NULL)
        {
			memset(buffer,0,50);	//清除缓冲区
            if(xQueueReceiveFromISR(Usart1_Queue1Handle,buffer,&xTaskWokenByReceive))//请求消息Message_Queue
            {
				
				printf("task3:%d \r\n",strlen((const char*)buffer));	
				for (i = 0;i<strlen((const char*)buffer);i++)
				printf("%c",buffer[i]);
            }

        }
        vTaskDelay(20);
  }
  /* USER CODE END Task3 */
}

/* USER CODE BEGIN Header_StartTask04 */
/**
* @brief Function implementing the Task4 thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartTask04 */
void StartTask04(void *argument)
{
  /* USER CODE BEGIN StartTask04 */
	uint8_t buffer[50];
	uint8_t i;
	BaseType_t xTaskWokenByReceive=pdFALSE;
  /* Infinite loop */
  for(;;)
  {

        if(Usart2_Queue1Handle!=NULL)
        {
			memset(buffer,0,50);	//清除缓冲区
            if(xQueueReceiveFromISR(Usart2_Queue1Handle,buffer,&xTaskWokenByReceive))//请求消息Message_Queue
            {
				printf("task4 \r\n");	
				for (i = 0;i<strlen((const char*)buffer);i++)
				printf("%c",buffer[i]);
            }

        }
        vTaskDelay(20);
  }
  /* USER CODE END StartTask04 */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */

/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
