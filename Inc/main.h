/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
/* ---------------------------Includes -----------------------------*/
/* ----------C标准库 ---------*/
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <ctype.h>
#include <stdbool.h>

/* ----------外设库 ---------*/
#include "config.h"
#include "gpio.h"
#include "log.h"
#include "usart.h"
#include "adc.h"
#include "spi.h"
#include "tim.h"
#include "common.h"

#include "24cxx.h"
#include "myiic.h"
#include "W25QXX.h"

/* ----------Freertos库 ---------*/
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"
#include "cmsis_os2.h"
/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/


/* USER CODE BEGIN Private defines */
/**********SPI CS引脚*************/
#define SPI1_CS_Pin GPIO_PIN_14
#define SPI1_CS_GPIO_Port GPIOB
#define SPI_CS_Enable() 			HAL_GPIO_WritePin(SPI1_CS_GPIO_Port, SPI1_CS_Pin, GPIO_PIN_RESET)
#define SPI_CS_Disable() 		    HAL_GPIO_WritePin(SPI1_CS_GPIO_Port, SPI1_CS_Pin, GPIO_PIN_SET)

/**********IIC引脚*************/
#define ST_I2C_SCL_Pin GPIO_PIN_8
#define ST_I2C_SCL_GPIO_Port GPIOB
#define ST_I2C_SDA_Pin GPIO_PIN_9
#define ST_I2C_SDA_GPIO_Port GPIOB


#define RxBuf_Size  1024     //最大接收字节数
extern unsigned char RxBuf[RxBuf_Size];   //接收数据
extern uint8_t aRxBuffer;   //接收中断缓冲



extern osMessageQueueId_t Usart1_Queue1Handle;
extern osMessageQueueId_t Usart2_Queue1Handle;




extern void bsp_delay_us(uint32_t us);
/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
